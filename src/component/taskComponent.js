import { Typography, Card, Grid, Tooltip } from "@material-ui/core";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import React from "react";
import translate from "../intl/translate";

const Task = ({ task, onDelete, onEdit }) => {
  return (
    <Grid item xl={3} md={4} sm={6} xs={12}>
      <Card>
        <Typography variant="h4" color="textPrimary" style={{ margin: "10px" }}>
          {translate ('Titre')}:{task.titre}
        </Typography>
        <Typography style={{ margin: "10px" }}>{translate('Date')}: {task.date}</Typography>
        <Typography style={{ margin: "10px" }}>
          {translate('Description')}: {task.description}
        </Typography>
        <Typography style={{ margin: "10px" }}>
          {translate('Status')}:{task.doneStatus}
        </Typography>

        <Tooltip arrow title="supprimer">
          <DeleteIcon
            style={{ margin: "10px", color: "red" }}
            onClick={() => onDelete(task.id)}
            className="delIcon"
          />
        </Tooltip>
        <Tooltip arrow title="editer">
          <EditIcon
            style={{ margin: "10px" }}
            color="primary"
            onClick={() => onEdit(task.id)}
            className="editIcon"
          />
        </Tooltip>
      </Card>
    </Grid>
  );
};

export default Task;
