import {React, useState } from 'react';
import Swal from "sweetalert2";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { IntlProvider, LOCALES } from '../intl';
import translate from '../intl/translate';

const AddTask = ({ onSave }) => {
    const [date, setDate] = useState(new Date());

    const formik = useFormik({
        initialValues: {
            titre: '',
            description: '',
            date: new Date(),
            doneStatus: 'en attente'
        },
        validationSchema: Yup.object({

            titre: Yup.string()
     
              .max(20, 'Must be 20 characters or less')
     
              .required('Required'),
     
            description: Yup.string()
     
              .max(1000, 'Must be 1000 characters or less')
     
              .required('Required'),
         
          }),
        onSubmit: values =>{
                onSave(values)
        },

        
    })


    return ( 
        <form  onSubmit = {formik.handleSubmit} >
        <label htmlFor='titre'> {translate ('tache')} </label> 
        <input 
        type = "text" 
        id='titre' 
        name='titre'
        value = {formik.values.titre }
        onChange = {formik.handleChange}/> 
        {formik.errors.titre ? <div>{formik.errors.titre}</div> : null}

        <div className = "form-control" >
        <label> {translate ('Date')} </label> 
        <DatePicker selected={date}
        onChange = {formik.handleChange}  showTimeSelect   dateFormat="MMMM eeee d, yyyy h:mm aa"/> 
        </div>

        <label> {translate('Description')} </label> 
        <input type = "text"
        id='description' name='description'
        placeholder = "description"
        value = { formik.values.description }
        onChange = {formik.handleChange }/> 
        {formik.errors.description ? <div>{formik.errors.description}</div> : null}


        <input type = "submit"
        className = "btn btn-block"
        value = "créer" />
        </form>
    )
}

export default AddTask
