import Header from "./Header";
import Task from "./taskComponent";
import AddTask from "./newTaskComponent";
import { React, useState, useEffect } from "react";
import { v4 as uuidv4 } from "uuid";
import Swal from "sweetalert2";
import "react-datepicker/dist/react-datepicker.css";
import { Typography, Card, Container, Grid } from "@material-ui/core";
import Footer from "./Footer";
import {IntlProvider, LOCALES} from "../intl";
import translate from '../intl/translate';


function App() {
  const [isPageLoading, setIsPageLoading] = useState(true);
  const [tasks, setTasks] = useState([]);
  const [isTaskFormVisible, setIsTaskFormVisible] = useState(false);

  useEffect(() => {
    if (getTasks == null) {
      setTasks([]);
    } else {
      setTasks(getTasks);
    }
    setTimeout(() => {
      setIsPageLoading(false);
    }, 3500);
    // eslint-disable-next-line
  }, []);

  const getTasks = JSON.parse(localStorage.getItem("taskAdded"));

  const addTask = (task) => {
    const id = uuidv4();
    const newTask = { id, ...task };

    setTasks([...tasks, newTask]);

    Swal.fire({
      icon: "success",
      title: "Youpi...",
      text: "Nouvelle Tache ajoutée!",
    });

    localStorage.setItem("taskAdded", JSON.stringify([...tasks, newTask]));
    window.location.reload();
  };

  const deleteTask = (id) => {
    const deleteTask = tasks.filter((task) => task.id !== id);

    setTasks(deleteTask);

    Swal.fire({
      icon: "success",
      title: "Youpi...",
      text: "Tache supprimée avec succès!",
    });

    localStorage.setItem("taskAdded", JSON.stringify(deleteTask));
  };

  const editTask = (id) => {
    const titre = prompt("Titre");
    const description = prompt("Description");
    const doneStatus = prompt("Statut: choisir entre en *attente* ou *fait* ");
    let data = JSON.parse(localStorage.getItem("taskAdded"));
    const myData = data.map((x) => {
      if (x.id === id) {
        return {
          ...x,
          titre: titre,
          date: new Date(),
          description: description,
          doneStatus: doneStatus,
          id: uuidv4(),
        };
      }
      return x;
    });

    Swal.fire({
      icon: "success",
      title: "Cool...",
      text: "Tache modifiée!",
    });

    localStorage.setItem("taskAdded", JSON.stringify(myData));
    window.location.reload();
  };

  return (
    <>
    <IntlProvider locale={LOCALES.FRENCH}>
      {isPageLoading ? (
        <div className="spinnerContainer">
          <div className="spinner-grow text-primary" role="status">
            <span className="visually-hidden"> {translate ('cours')} ...</span>
          </div>
        </div>
      ) : (
        <Container style={{ marginTop: "100px" }} maxWidth>
          <Header
            showForm={() => setIsTaskFormVisible(!isTaskFormVisible)}
            changeTextAndColor={isTaskFormVisible}
          />
          {isTaskFormVisible && <AddTask onSave={addTask} />}
          <Typography variant="h4" align="center">
          {translate('nombre_tache')}
            : {tasks.length}
          </Typography>

          <Card>
            <Grid container spacing={4} style={{ margin: "10px" }}>
              {tasks.length > 0
                ? tasks.map((task, index) => (
                    <Task
                      key={index}
                      task={task}
                      onDelete={deleteTask}
                      onEdit={editTask}
                    />
                  ))
                : "Tache pas trouvée!"}
            </Grid>
          </Card>
        </Container>
      )}
      <Footer />
    </IntlProvider>
    </>
  );
}

export default App;
