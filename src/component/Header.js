import React from 'react';
import "../index.css";
import Button from './Button';
import ImageAvatars from './avatar'
import { Typography, CssBaseline, Toolbar, AppBar } from '@material-ui/core';
import AccessTimeFilledOutlinedIcon from '@mui/icons-material/AccessTimeFilledOutlined';
import translate from '../intl/translate';

const Header = ({ showForm, changeTextAndColor }) => {

    return (
       <>
        <CssBaseline/>
        <AppBar>
            <Toolbar position="relative">
                <AccessTimeFilledOutlinedIcon/>
                <Typography variant="h3" align='center'>Agendr</Typography>
                <ImageAvatars/>
            </Toolbar>
            <Button  onClick={showForm} color={changeTextAndColor ? 'white' : 'white'} text={changeTextAndColor ? 'X Fermer' : '+ Ajouter'} />
        </AppBar>
       
       </>
    )
}

export default Header;
