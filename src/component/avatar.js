import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import eng from '../static/images/eng.jpeg';
import fren from '../static/images/fren.jpeg';
import {IntlProvider, LOCALES} from "../intl";

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
}));

export default function ImageAvatars() {
  const classes = useStyles();
  const [locale, setLocale] = useState()

  return (
    <IntlProvider locale={locale}>
        <div className={classes.root}>
            <button onClick={() =>setLocale(LOCALES.ENGLISH)}><Avatar alt="Remy Sharp"   src={eng} className={classes.small} /></button>
            <button onClick={() =>setLocale(LOCALES.FRENCH)}><Avatar alt="Remy Sharp"    src={fren} className={classes.small} /></button>
        </div>
    </IntlProvider>
  );
}