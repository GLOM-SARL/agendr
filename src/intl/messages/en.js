import { LOCALES } from "../locales"

export default {
    [LOCALES.ENGLISH]: {
        'edit' : 'Edite {path} ',
        'nombre_tache' : 'Number of task',
        'cours' : 'loading',
        'tache' : 'task',
        'Description' : 'Description',
        'description' : 'description',
        'Date' : 'Date',
        'date':  'date',
        'creer' : 'create',
        'Titre': 'title',
        'Status' : 'Status',
        'XF' : 'X Close',
        '+A' : '+ Add',
    }
}