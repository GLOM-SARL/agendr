import { LOCALES } from "../locales";

export default {
    [LOCALES.FRENCH]:{
        'nombre_tache' : 'Nombre de tache',
        'cours' : 'en cours',
        'tache' : 'tache',
        'Description' : 'Description',
        'description' : 'description',
        'Date' : 'Date',
        'date':  'date',
        'creer' : 'créer',
        'Titre' : 'Titre',
        'Status' : 'Statut',
        'XF' : 'X Fermer',
        '+A' : '+ Ajouter',
    }
}